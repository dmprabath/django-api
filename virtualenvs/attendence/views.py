from django.shortcuts import render
from django.core import serializers
import requests,json
import datetime
db_config = {
    "username": "prabath@codeso.lk",
    "password": "1234",
    "db": "prabath"
}
headers = {
    'content-type': 'application/x-www-form-urlencoded',
    'charset':'utf-8'
}

employee_filters = {
    "field": "['id', 'name','barcode']",
    "order": 'barcode',
}
#openpgpwd
server_url = "http://127.0.0.1:7073" #server address

get_token_url = "/api/auth/get_tokens"
auth_url = server_url + get_token_url
employee_api_url = "/api/hr.employee"
attendence_api_url ="/api/hr.attendance"
get_employee = server_url + employee_api_url
post_attendence_url = server_url + attendence_api_url


def Home(request):
    req=requests.post(url=auth_url, data=db_config,headers=headers)
    content = json.loads(req.content.decode('utf-8'))
    headers['access-token'] = content.get('access_token')
    print(content.get('access_token'))
    req_emp=requests.get(url=get_employee, data=employee_filters,headers=headers)
    employee_content=json.loads(req_emp.content.decode('utf-8'))
    
    employee_list = employee_content["results"]
    for x in employee_list:
        print(x["id"])
    one_emp =employee_list[0]
    print(one_emp["id"])
    print(one_emp["name"])
    count_x = 1
    if request.method == 'POST':
        #print(request.POST['name'])
        #request.POST['name']
            
        id_list=request.POST.getlist('id')
        name_list=request.POST.getlist('name')
        check_in_list=request.POST.getlist('in')
        check_out_list=request.POST.getlist('out')
        selected_date=request.POST['date']
        n_shift_list = request.POST.getlist('shift')
        
        print(id_list)
        print(name_list)
        print(check_in_list)
        print(check_out_list)
        print(selected_date)
        print(n_shift_list)
        #for id_count in id_list:
        #    attendence_data = {
        #        "employee_id": id_count,
        #                }
        #    for in_count in check_in_list:
        #        attendence_data['check_in']=selected_date+" "+in_count
        #        for out_count in check_out_list:
        #            attendence_data['check_out']=selected_date+" "+out_count
        #            print(attendence_data)
        
        count_x = 0
        loop_count=(len(id_list))
        while count_x < loop_count:
            if n_shift_list[count_x]=="n_shift":
                date_time_str = (str(selected_date))
                date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d')
                day1=date_time_obj.date()
                end_date = day1 + datetime.timedelta(days=1)
                check_off_day = end_date
            else:
                check_off_day=selected_date
            

            time_sets = check_in_list[count_x]
            time_zone_cal = time_sets
            print("efdeffeeffefefefefeffefefefefefeefefefefeffefefefefeefefef",time_zone_cal)

            #time_object = datetime.datetime.strptime(time_zone_cal, '%H:%M').time()
            tz_time_obj = datetime.datetime.strptime(time_zone_cal, "%H:%M")  
            check_in_tz =  tz_time_obj - datetime.timedelta(hours=5.5)
            print("time zoneeeeeeeeeeeeeeeeeeeeee",check_in_tz)
            check_in_formated =  (check_in_tz.strftime("%H:%M"))
            print("format time",check_in_formated)
            #print()
            time_sets2 = check_out_list[count_x]
            time_zone_cal2 = time_sets2
            tz_time_obj2 = datetime.datetime.strptime(time_zone_cal2, "%H:%M")  
            check_in_tz2 =  tz_time_obj2 - datetime.timedelta(hours=5.5)
            print("time zoneeeeeeeeeeeeeeeeeeeeee",check_in_tz2)
            check_out_formated =  (check_in_tz2.strftime("%H:%M"))
            print("format time",check_out_formated)
            
                
            attendence_data = {
                "employee_id":id_list[count_x],
                "check_in":selected_date +" "+ check_in_formated,
                "check_out":(str(check_off_day)) +" "+ check_in_formated,
               # "tz":"Asia/Colombo"
            }
            print(attendence_data)
            req=requests.post(url=post_attendence_url, data=attendence_data,headers=headers)
            print(req)
            count_x = count_x+1
        
    
    return render(request, 'home.html',{"con":employee_list})


def Login(request): 
    
    return render(request, 'login.html',{"msg":"Next Employee","msgtype":'1'})
            
    #return render(request,'login.html')


def Loginval(request, qid):   
    if request.method =="POST":
        EMP_ID = request.POST["employeeid"]
        EPF_NO = request.POST["con_login_id"]
        today = datetime.date.today()
        today = today.strftime("%Y-%m-%d")


        req=requests.post(url=auth_url, data=db_config,headers=headers)
        content = json.loads(req.content.decode('utf-8'))
        headers['access-token'] = content.get('access_token')        
        attendance_filters = {
            "field": "['id','employee_id', 'check_in','check_out']",
            "order": 'check_in',
        }
        req_attendance=requests.get(url=post_attendence_url, data=attendance_filters, headers=headers)
        if req_attendance:
            attendance_content=json.loads(req_attendance.content.decode('utf-8'))   
            attendance_list = attendance_content["results"]
            for emp in attendance_list:
                j_date=emp['check_in'].split()
                j_date =j_date[0]
                if int(EMP_ID) == emp['employee_id'][0]:
                    if(today == j_date and emp['check_out']==""  ):
                        #check_in = str(emp['check_in']).strftime("%Y-%m-%d")
                        #if found check_in time for today
                        check_out = datetime.datetime.now()
                        check_out = check_out.strftime("%Y-%m-%d %H:%M:%S")
                        #print(check_out)
                        check_out_data={
                            'employee_id':emp['employee_id'][0],
                            'check_out':check_out,
                            'id':emp['id']
                        }
                        
                        fill = requests.put(url=post_attendence_url, data=check_out_data, headers=headers)
                        print(fill)
                        break
                    else:
                        check_in_time = datetime.datetime.now()
                        check_in_time = check_in_time.strftime("%Y-%m-%d %H:%M:%S")
                        #print(check_out)
                        check_in_data={
                            'employee_id':emp['employee_id'][0],
                            'check_in':check_in_time,
                            
                        }
                        
                        fill = requests.post(url=post_attendence_url, data=check_in_data, headers=headers)
                        print(fill)
                        break
            return render(request, 'login.html',{"msg":"Next Employee", 'msgtype':'1'}) 
        else:
            check_in_time = datetime.datetime.now()
            check_in_time = check_in_time.strftime("%Y-%m-%d %H:%M:%S")
                        #print(check_out)
            check_in_data={
                'employee_id':EMP_ID,
                'check_in':check_in_time,
                            
            }
                        
            fill = requests.post(url=post_attendence_url, data=check_in_data, headers=headers)
            print(fill)
            return render(request, 'login.html',{"msg":"Next Employee", 'msgtype':'1'}) 
         

    else:
        req=requests.post(url=auth_url, data=db_config,headers=headers)
        content = json.loads(req.content.decode('utf-8'))
        headers['access-token'] = content.get('access_token')        
        req_emp=requests.get(url=get_employee, data=employee_filters, headers=headers)
        employee_content=json.loads(req_emp.content.decode('utf-8'))  
        employee_list = employee_content["results"]
        for emp in employee_list:
            epfid = emp["barcode"] 
            if str(qid) == epfid:
                context={
                    "pin": qid,
                    "employeeid":emp["id"] 
                } 
                return render(request, 'login.html',{"context":context, "msg":"Again type EPF number","msgtype":'1'})
                break
            
        return render(request, 'login.html',{"msg":"Sorry You are Wrong Persons"})  
 
            
                
     

    
       

    

   
    




# def Data(request):
#     json_serializer = serializers.get_serializer("json")()
# companies = json_serializer.serialize(Company.objects.all().order_by('id')[:5], ensure_ascii=False)